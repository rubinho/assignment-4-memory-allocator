#ifndef MEMORY_ALLOCATOR_MAIN_H
#define MEMORY_ALLOCATOR_MAIN_H
typedef enum {
    TEST_SUCCESS = 0,
    TEST_ONE,
    TEST_TWO,
    TEST_EXTEND_OK,
    TEST_EXTEND_FAIL,
} tests;

#endif
