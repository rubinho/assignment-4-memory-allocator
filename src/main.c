#define _DEFAULT_SOURCE

#include "main.h"
#include "mem.h"
#include "mem_debug.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <sys/mman.h>


const char *get_error_message(const char *message) {
    debug(message);
    return "ASSERTION FAILED";
}


static void test_alloc() {
    void *heap = heap_init(0);
    debug("Before:");
    debug_heap(stdout, heap);

    void *memory = _malloc(256);
    debug("After:");
    debug_heap(stdout, heap);

    assert(memory != NULL && get_error_message("Failed allocation"));

    _free(memory);

    debug("After freeing memory:");
    debug_heap(stdout, heap);

    heap_term();
}

static void test_one() {
    void *heap = heap_init(0);
    debug("Before:");
    debug_heap(stdout, heap);

    void *first = _malloc(256);
    void *second = _malloc(512);

    debug("After:");
    debug_heap(stdout, heap);

    assert(first != NULL && get_error_message("Fail with first block"));
    assert(second != NULL && get_error_message("Fail with second block"));

    _free(first);
    _free(second);

    heap_term();

}


static void test_two() {
    void *heap = heap_init(0);
    debug("Before:");
    debug_heap(stdout, heap);

    void *first = _malloc(128);
    void *second = _malloc(256);
    void *third = _malloc(512);

    puts("State after:");
    debug_heap(stdout, heap);

    assert(first != NULL && get_error_message("Fail with first block"));
    assert(second != NULL && get_error_message("Fail with second block"));
    assert(third != NULL && get_error_message("Fail with third block"));

    _free(first);
    _free(second);

    debug("After freeing:");
    debug_heap(stdout, heap);

    _free(third);

    heap_term();

}

static void test_extension_ok() {
    struct region *heap = heap_init(0);
    debug("Before:");
    debug_heap(stdout, heap);


    void *first = _malloc(REGION_MIN_SIZE);
    void *second = _malloc(REGION_MIN_SIZE * 4);

    assert(first != NULL && get_error_message("Fail with first region"));
    assert(second != NULL && get_error_message("Fail with second region"));

    assert(second - REGION_MIN_SIZE -
           offsetof(struct block_header, contents) == first && get_error_message("Fail with region extension"));

    debug("After:");
    debug_heap(stdout, heap);

    _free(first);
    _free(second);

    heap_term();

}

static void test_extension_fail() {
    heap_init(0);
    void *mmap_addr = map_pages(HEAP_START + REGION_MIN_SIZE, REGION_MIN_SIZE, MAP_FIXED);

    assert(mmap_addr != MAP_FAILED && get_error_message("Fail with allocation"));

    debug("Before:");
    debug_heap(stdout, HEAP_START);

    void *first = _malloc(REGION_MIN_SIZE);
    void *second = _malloc(REGION_MIN_SIZE * 2);
    debug("After:");
    debug_heap(stdout, HEAP_START);

    void *first_header = block_get_header(first);
    assert(first_header == HEAP_START && get_error_message("First region extension fail"));

    void *second_header = block_get_header(second);
    assert(second_header != HEAP_START + REGION_MIN_SIZE && get_error_message("Second region extension fail"));


    _free(first);
    _free(second);

    heap_term();

}

static void (*do_test[])() = {
        [TEST_SUCCESS] = test_alloc,
        [TEST_ONE] = test_one,
        [TEST_TWO] = test_two,
        [TEST_EXTEND_OK] = test_extension_ok,
        [TEST_EXTEND_FAIL] = test_extension_fail
};


int main() {
    do_test[TEST_SUCCESS]();
    do_test[TEST_ONE]();
    do_test[TEST_TWO]();
    do_test[TEST_EXTEND_OK]();
    do_test[TEST_EXTEND_FAIL]();
}